import React from 'react';

import './About.css';

const About = () => {
  return (
    <div className="about">
      <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ol className="about__profile">
        <li><strong>Nama:</strong> Yohanzen Christanto Alexander</li>
        <li><strong>Email:</strong> yohanzen.alexander@gmail.com</li>
        <li><strong>Sistem Operasi yang digunakan:</strong> Windows</li>
        <li><strong>Akun Gitlab:</strong> coposaja</li>
        <li><strong>Akun Telegram:</strong> coposaja</li>
      </ol>
    </div>
  )
}

export default About

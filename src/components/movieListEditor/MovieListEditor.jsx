import Axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../auth/AuthContext';
import MovieForm from './MovieForm';
import MovieList from './MovieList';

const MovieListEditor = (props) => {
  const [movieList, setMovieList] = useState([]);
  const [selectedId, setSelectedId] = useState(-1);
  const [auth] = useContext(AuthContext);

  useEffect(() => {
    if (!auth) props.history.push('/login')
    else getMovies();
  }, [])

  const getMovies = () => {
    Axios.get('http://backendexample.sanbercloud.com/api/movies')
      .then(res => {
        setMovieList(res.data);
      })
      .catch(err => console.log(err));
  }

  const onDeleteHandler = (id) => {
    Axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
      .then(res => {
        getMovies();
      })
      .catch(err => console.log(err));
  }

  const onSubmitHandler = (movie) => {
    if (selectedId) {
      Axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, movie)
        .then(res => {
          getMovies();
        })
        .catch(err => console.log(err));
    }
    else {
      Axios.post('http://backendexample.sanbercloud.com/api/movies', movie)
        .then(res => {
          getMovies();
        })
        .catch(err => console.log(err));
    }

    setSelectedId(0);
  }

  const onEditHandler = (id) => {
    setSelectedId(id);
  }

  const onSearchHandler = (keyword) => {
    Axios.get('http://backendexample.sanbercloud.com/api/movies')
      .then(res => {
        setMovieList(res.data.filter((x) => x.title.includes(keyword)));
      })
      .catch(err => console.log(err));
  }

  return (
    <div style={{ width: '80%', margin: 'auto' }}>
      <MovieList
        movies={movieList}
        onDeleteHandler={onDeleteHandler}
        onEditHandler={onEditHandler}
        onSearchHandler={onSearchHandler}
      />
      <MovieForm
        onSubmitHandler={onSubmitHandler}
        selectedMovie={movieList.find((x) => x.id === selectedId)}
      />
    </div>
  )
}

export default MovieListEditor;

import React, { useEffect, useState } from 'react';

import './MovieForm.css';

const MovieForm = (props) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [year, setYear] = useState(2020);
  const [duration, setDuration] = useState(120);
  const [genre, setGenre] = useState('');
  const [rating, setRating] = useState(0);
  const [imageUrl, setImageUrl] = useState('');
  const [errMessage, setErrMessage] = useState([]);

  useEffect(() => {
    if (props.selectedMovie) {
      const movie = props.selectedMovie;
      console.log(movie);
      setTitle(movie.title);
      setDescription(movie.description);
      setYear(movie.year);
      setDuration(movie.duration);
      setGenre(movie.genre);
      setRating(movie.rating);
      setImageUrl(movie.image_url);
    }
  }, [props.selectedMovie])

  const onYearChange = (e) => {
    const year = isNaN(e.target.value) ? 2020 : +e.target.value;
    setYear(year);
  }

  const onDurationChange = (e) => {
    const duration = isNaN(e.target.value) ? 120 : +e.target.value;
    setDuration(duration);
  }

  const onRatingChange = (e) => {
    const rating = isNaN(e.target.value) ? 0 : Math.min(10, +e.target.value);
    setRating(rating);
  }

  const submitFormHandler = () => {
    if (!validateForm()) return;

    const movie = {
      title: title,
      description: description,
      year: year,
      duration: duration,
      genre: genre,
      rating: rating,
      image_url: imageUrl,
    };

    props.onSubmitHandler(movie);
    resetForm();
  }

  const validateForm = () => {
    const errMessages = [];
    if (year < 1980) errMessages.push('Year minimum is 1980');

    setErrMessage(errMessages);
    return errMessages.length === 0;
  }

  const resetForm = () => {
    setTitle('');
    setDescription('');
    setYear(2020);
    setDuration(120);
    setGenre('');
    setRating(0);
    setImageUrl('');
  }

  return (
    <div className="movieform">
      <h1>Movies Form</h1>

      <div className="movieform__form">
        <div className="movieform__input-group">
          <label htmlFor="txtTitle">Title</label>
          <input
            id="txtTitle"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>

        <div className="movieform__input-group">
          <label htmlFor="txtDescription">Description</label>
          <textarea
            cols={20}
            rows={2}
            id="txtDescription"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          ></textarea>
        </div>

        <div className="movieform__input-group">
          <label htmlFor="numYear">Year</label>
          <input
            id="numYear"
            name="year"
            value={+year}
            onChange={onYearChange}
          />
        </div>

        <div className="movieform__input-group">
          <label htmlFor="numDuration">Duration</label>
          <input
            id="numDuration"
            name="duration"
            value={+duration}
            onChange={onDurationChange}
          />
        </div>

        <div className="movieform__input-group">
          <label htmlFor="txtGenre">Genre</label>
          <input
            id="txtGenre"
            name="genre"
            value={genre}
            onChange={(e) => setGenre(e.target.value)}
          />
        </div>

        <div className="movieform__input-group">
          <label htmlFor="numRating">Rating</label>
          <input
            id="numRating"
            name="rating"
            value={+rating}
            onChange={onRatingChange}
          />
        </div>

        <div className="movieform__input-group">
          <label htmlFor="txtUrl">Image Url</label>
          <textarea
            cols={50}
            rows={5}
            id="txtUrl"
            value={imageUrl}
            onChange={(e) => setImageUrl(e.target.value)}
          ></textarea>
        </div>

        <ul className="movieform__error-message">
          {errMessage.map((err, index) => (
            <li key={index}>
              {err}
            </li>
          ))}
        </ul>

        <button onClick={() => submitFormHandler()}>
          Submit
        </button>
      </div>
    </div>
  )
}

export default MovieForm

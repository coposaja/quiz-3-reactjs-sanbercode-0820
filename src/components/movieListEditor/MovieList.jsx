import React, { useState } from 'react';

import './MovieList.css';

const MovieList = (props) => {
  const [search, setSearch] = useState('');
  return (
    <div className="movielist">
      <div className="movielist__search">
        <input
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <button onClick={() => props.onSearchHandler(search)}>Search</button>
      </div>

      <h1>Daftar Film</h1>
      <table className="movielist__table">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {props.movies.length ?
            props.movies.map((movie, index) => (
              <tr key={movie.id}>
                <td>{index + 1}</td>
                <td>{movie.title}</td>
                <td>
                  <span>
                    {movie.description}
                  </span>
                </td>
                <td>{movie.year}</td>
                <td>{movie.duration}</td>
                <td>{movie.genre}</td>
                <td>{movie.rating}</td>
                <td>
                  <button onClick={() => props.onEditHandler(movie.id)}>EDIT</button>
                  <button onClick={() => props.onDeleteHandler(movie.id)}>DELETE</button>
                </td>
              </tr>
            )) : (
              <tr>
                <td colSpan={8}>No Data</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </div>
  )
}

export default MovieList

import React, { useContext } from 'react';

import './Header.css';
import LogoImg from '../../assets/images/logo.png';
import { NavLink } from 'react-router-dom';
import { AuthContext } from '../auth/AuthContext';

const Header = () => {
  const [auth] = useContext(AuthContext);
  return (
    <div className="header">
      <img
        className="header__logo"
        src={LogoImg} width="200px"
        alt="sanber logo"
      />
      <nav>
        <ul>
          <li><NavLink to="/" exact>Home</NavLink></li>
          <li><NavLink to="/About">About</NavLink></li>
          {!auth && <li><NavLink to="/Login">Login</NavLink></li>}
          {auth && (
            <>
              <li><NavLink to="/Movies">Movie List Editor</NavLink></li>
              <li><NavLink to="/Login">Logout</NavLink></li>
            </>
          )}
        </ul>
      </nav>
    </div>
  )
}

export default Header

import React from 'react';

import './Contact.css'

const Contact = () => {
  return (
    <div className="contact">
      <h1>Hubungi Kami</h1>
      <strong>Kantor :</strong> di jalan belum jadi
      <br />
      <strong>Nomor Telepon :</strong> 08XX-XXXX-XXXX
      <br />
      <strong>Email :</strong> email@contoh.com
      <h1>Kirimkan Pesan</h1>

      <div className="contact__form">
        <div className="contact__input-group">
          <label for="txtName">Nama</label>
          <input type="text" id="txtName" name="Name" />
        </div>

        <div className="contact__input-group">
          <label for="txtEmail">Email</label>
          <input type="email" id="txtEmail" name="Email" />
        </div>

        <div className="contact__input-group">
          <label>Jenis Kelamin</label>
          <div className="contact__input-gender">
            <input type="radio" id="male" name="gender" value="male" />
            <label for="male">Laki-laki</label><br />
            <input type="radio" id="female" name="gender" value="female" />
            <label for="female">Perempuan</label><br />
          </div>
        </div>

        <div className="contact__input-group">
          <label for="txtMessage">Pesan Anda</label>
          <textarea cols={50} rows={5} id="txtMessage"></textarea>
        </div>

        <button>Kirim</button>
      </div>
    </div>
  )
}

export default Contact

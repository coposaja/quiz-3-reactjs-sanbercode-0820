import React, { useContext, useEffect, useState } from 'react';

import './Auth.css';
import { AuthContext } from './AuthContext';

const Auth = (props) => {
  const [auth, setAuth] = useContext(AuthContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (auth) {
      setAuth('');
      localStorage.removeItem('auth');
    }
  }, [])

  const onLogin = () => {
    if (username && password) {
      setAuth(username);
      localStorage.setItem('auth', username);
      props.history.push('/')
    }
  }

  return (
    <div className="auth">
      <div className="auth__input-group">
        <label htmlFor="txtUsername">Username</label>
        <input
          type="text"
          id="txtUsername"
          name="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </div>
      <div className="auth__input-group">
        <label htmlFor="txtPassword">Password</label>
        <input
          type="password"
          id="txtPassword"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>

      <button onClick={onLogin}>
        Login
      </button>
    </div>
  )
}

export default Auth

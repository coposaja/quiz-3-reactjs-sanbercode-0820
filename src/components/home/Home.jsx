import Axios from 'axios';
import React, { Component } from 'react';

import './Home.css';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      movies: [],
    }
  }

  componentDidMount() {
    Axios.get('http://backendexample.sanbercloud.com/api/movies')
      .then(res => {
        this.setState({ movies: res.data })
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <div className="home">
        <h1>Daftar Film Film Terbaik</h1>
        <div className="home__articles">
          {this.state.movies.map((movie) => (
            <div className="home__article" key={movie.id}>
              <h2>{movie.title}</h2>
              <div className="home__article__detail">
                <img src={movie.image_url} alt="movie poster" />
                <span>
                  <strong>Rating: {movie.rating}</strong> <br />
                  <strong>Durasi: {movie.duration}</strong> <br />
                  <strong>Genre: {movie.genre}</strong> <br />
                </span>
              </div>
              <div className="home__article__description">
                <p>
                  <strong>Deskripsi: </strong>
                  {movie.description}
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}

export default Home;

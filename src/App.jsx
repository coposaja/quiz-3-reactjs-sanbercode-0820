import React, { useContext, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import About from './components/about/About';
import Auth from './components/auth/Auth';
import { AuthContext } from './components/auth/AuthContext';
import Contact from './components/contact/Contact';
import Header from './components/header/Header';
import Home from './components/home/Home';
import MovieListEditor from './components/movieListEditor/MovieListEditor';

const App = () => {
  const [, setAuth] = useContext(AuthContext);

  useEffect(() => {
    const session = localStorage.getItem('auth');
    if (session) setAuth(session);
  }, [])
  return (
    <div className="app">
      <Header />
      <div className="app__content">
        <Switch>
          <Route path="/Movies" component={MovieListEditor} />
          <Route path="/About" component={About} />
          <Route path="/Contact" component={Contact} />
          <Route path="/" exact component={Home} />
          <Route path="/login" component={Auth} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
